package project;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.scene.shape.Circle;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import javafx.scene.layout.Pane;

public class Principal extends Application {
    String opcion = "";
    
    double xincrement = 0;
    double yincrement = 0;
    Circle circle;
    
    int coord_x1 = 0;
    int coord_y1 = 0;
    
    int coord_x2 = 0;
    int coord_y2 = 0;
    
    int coord_last_x = 0;
    int coord_last_y = 0;
    
    int cont_clicks = 1;
    @Override
    public void start(Stage primaryStage) {
        Group group = new Group();
        Pane root = new Pane();
        
        Button btnTrazo = new Button();
        btnTrazo.setLayoutX(100);
        btnTrazo.setLayoutY(100);
        btnTrazo.setMinSize(50, 100);
        btnTrazo.setText("Trazo");
        
        Button btnRectangulo = new Button();
        btnRectangulo.setLayoutX(100);
        btnRectangulo.setLayoutY(300);
        btnRectangulo.setMinSize(50, 100);
        btnRectangulo.setText("Rectangulo");
        
        root.getChildren().add(group);
        root.getChildren().add(btnRectangulo);
        root.getChildren().add(btnTrazo);
        Scene scene = new Scene(root, 1080, 720);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
        Queue<Circle> puntos = new LinkedList<Circle>();
        
        btnRectangulo.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				opcion = "Rectangulo";
				cont_clicks = 1;
			}
        });
        btnTrazo.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent arg0) {
				cont_clicks = 1;
				opcion = "Trazo";
			}
        });
        
        scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
        	
        	
        	
            @Override
            public void handle(MouseEvent mouseEvent) {
            	if(opcion =="Rectangulo") {
            		circle = new Circle();
                    
                    if(cont_clicks == 1) {
                    	coord_x1 =(int) mouseEvent.getX();
                    	coord_y1 =(int) mouseEvent.getY();
                    	cont_clicks = 2;
                    }else if(cont_clicks == 2) {
                    	coord_x2 =(int) mouseEvent.getX();
                    	coord_y2 =(int) mouseEvent.getY();
                    	
                    	pintarRectangulo(coord_x1, coord_y1, coord_x2, coord_y2, group );
                    	cont_clicks = 1;
                    }
                    
            	}else{
            		circle = new Circle();
                    if(cont_clicks == 1) {
                    	coord_x1 =(int) mouseEvent.getX();
                    	coord_y1 =(int) mouseEvent.getY();
                    	cont_clicks = 2;
                    }else if(cont_clicks == 2) {
                    	coord_x2 =(int) mouseEvent.getX();
                    	coord_y2 =(int) mouseEvent.getY();
                    	pintarTrazo(coord_x1, coord_y1, coord_x2, coord_y2, group );
                    	cont_clicks = 3;
                    }else if (cont_clicks == 3) {
                    	coord_last_x = coord_x2;
                    	coord_last_y = coord_y2;
                    	coord_x2 =(int) mouseEvent.getX();
                    	coord_y2 =(int) mouseEvent.getY();
                    	pintarTrazo(coord_last_x, coord_last_y, coord_x2, coord_y2, group );
                    	coord_last_x = coord_x2;
                    	coord_last_y = coord_y2;
                    }
            	}
            }
        });
        
        scene.setOnKeyReleased(event -> {
	    	if (event.getCode() == KeyCode.ENTER){
    			System.out.println("Enter");
    			pintarTrazo(coord_last_x, coord_last_y, coord_x1, coord_y1, group );
    		}
		});
        
    }

    public void pintarTrazo(int coord_x1, int coord_y1, int coord_x2, int coord_y2,Group group ){
    	System.out.println("X1: "+coord_x1+"\tY1: "+coord_y1+"\nX2: "+coord_x2+"\tY2: "+coord_y2);
    	int distancia_x = Math.abs(coord_x2 - coord_x1);
    	int distancia_y = Math.abs(coord_y2 - coord_y1);
    	double x = coord_x1;
    	double y = coord_y1;
    	double proporcion_x = 0;
    	double proporcion_y = 0;
    	
    	if(distancia_x > distancia_y) {
    		proporcion_x = (double)(coord_x2 - coord_x1) / (double)distancia_x;
    		proporcion_y = (double)(coord_y2 - coord_y1) / (double)distancia_x;
    		for(int i = 0; i < distancia_x; i++){
                circle = new Circle();
                x += proporcion_x;
                y += proporcion_y;
                //System.out.println(x+":"+y);
                circle.setCenterX(x);
                circle.setCenterY(y);
                circle.setRadius(2.0f);
                group.getChildren().addAll(circle);
            }
    	}else {
    		proporcion_x = (double) (coord_x2 - coord_x1) / (double)distancia_y;
    		proporcion_y = (double) (coord_y2 - coord_y1) / (double)distancia_y;
    		for(int i = 0; i < distancia_y; i++){
                circle = new Circle();
                x += proporcion_x;
                y += proporcion_y;
                //System.out.println(x+":"+y);
                circle.setCenterX(x);
                circle.setCenterY(y);
                circle.setRadius(2.0f);
                group.getChildren().addAll(circle);
            }
    	}
    }
    
    public void pintarRectangulo(int coord_x1, int coord_y1, int coord_x2, int coord_y2,Group group ){
    	System.out.println("X1: "+coord_x1+"\tY1: "+coord_y1+"\nX2: "+coord_x2+"\tY2: "+coord_y2);
    	int distancia_x = Math.abs(coord_x2 - coord_x1);
    	int distancia_y = Math.abs(coord_y2 - coord_y1);
    	int x = 0;
    	int y = 0;

    	

    		
    		if(coord_x1 > coord_x2) {
    			x = coord_x2;
    		}else {
    			x = coord_x1;
    		}
    		if(coord_y1 < coord_y2) {
    			y = coord_y1;
    		}else {
    			y = coord_y2;
    		}
    		System.out.println("DistX: "+distancia_x+"\tDistY: "+distancia_y);
    		for(int i = x; i < (distancia_x + x) ; i++){
                circle = new Circle();
                
                circle.setCenterX(i);
                circle.setCenterY(y);
                circle.setRadius(2.0f);
                group.getChildren().addAll(circle);
                
                circle = new Circle();
                circle.setCenterX(i);
                circle.setCenterY(y+distancia_y);
                circle.setRadius(2.0f);
                group.getChildren().addAll(circle);
            }
    		

    		for(int i = y; i < (distancia_y + y); i++){
                circle = new Circle();
                circle.setCenterX(x);
                circle.setCenterY(i);
                circle.setRadius(2.0f);
                group.getChildren().addAll(circle);
                
                circle = new Circle();
                circle.setCenterX(x+distancia_x);
                circle.setCenterY(i);
                circle.setRadius(2.0f);
                group.getChildren().addAll(circle);
            }
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}

